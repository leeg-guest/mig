#!/usr/bin/make -f
#
# Based on the work by Marcus Brinkmann <brinkmd@debian.org>
# Rewritten by Guillem Jover <guillem@debian.org>
# Rewritten to use `dh' by Pino Toscano <pino@debian.org>
#

export DEB_CFLAGS_MAINT_PREPEND := -Wall -pipe

DEB_HOST_ARCH := $(shell dpkg-architecture -qDEB_HOST_ARCH)
DEB_TARGET_ARCH := $(shell dpkg-architecture -qDEB_TARGET_ARCH)

DEB_BUILD_GNU_TYPE := $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_GNU_TYPE := $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_TARGET_GNU_TYPE := $(shell dpkg-architecture -qDEB_TARGET_GNU_TYPE)

%:
	dh $@ -Bbuild --with autoreconf

ifeq ($(DEB_HOST_GNU_TYPE),$(DEB_TARGET_GNU_TYPE))
ARCH=any-i386
else
CONFIGURE_OPTIONS = \
	  --build=$(DEB_BUILD_GNU_TYPE) \
	  --host=$(DEB_HOST_GNU_TYPE) \
	  --target=$(DEB_TARGET_GNU_TYPE)
CROSS=-$(DEB_TARGET_GNU_TYPE)
CROSSDEP=:$(DEB_TARGET_ARCH)
ARCH=$(DEB_HOST_ARCH)
# The cross compiler at this stage does not have these in its standard include
export TARGET_CPPFLAGS = -I/usr/include -I/usr/include/$(DEB_TARGET_GNU_TYPE)
endif
debian/control: debian/control.stamp
debian/control.stamp: debian/control.in
	sed -e 's/@cross@/$(CROSS)/' \
	    -e 's/@crossdep@/$(CROSSDEP)/' \
	    -e 's/@arch@/$(ARCH)/' \
	    < $< > debian/control
ifneq ($(DEB_HOST_GNU_TYPE),$(DEB_TARGET_GNU_TYPE))
	sed -i -e '/^Multi-Arch: /d' debian/control
endif
	touch $@

override_dh_auto_configure:
	dh_auto_configure -Bbuild -- \
	  --libexecdir=/usr/lib/mig/ \
	  $(CONFIGURE_OPTIONS)

ifneq ($(DEB_HOST_GNU_TYPE),$(DEB_TARGET_GNU_TYPE))
build-arch: debian/control.stamp
build-indep:
build: build-indep build-arch
endif

ifeq ($(DEB_HOST_GNU_TYPE),$(DEB_TARGET_GNU_TYPE))
# Make sure we always also have the gnu type alias, like gcc
override_dh_auto_install:
	dh_auto_install
	ln -sf mig debian/mig/usr/bin/$(DEB_TARGET_GNU_TYPE)-mig
endif

override_dh_auto_clean:
	dh_auto_clean
	rm -rf build-aux debian/control.stamp

